# Download csv data and extract
# https://www.ecofoci.noaa.gov/projects/IPHC/efoci_IPHCData.shtml

# url.txt from R script
# -- note some (~5) files were dead/missing links

# cd to input directory
cd data

# add url list as variable
urls=$(<urls.txt)

for url in $urls; do
    filename=${url##*/}
    echo ${filename}
    if [[ ! -f "$filename" ]]; then
        wget --no-hsts --no-check-certificate ${url}
        tar -xvzf $filename
    fi
done
