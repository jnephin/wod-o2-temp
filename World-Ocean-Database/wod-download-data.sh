# Download CTD and OSD data from WOD
# https://www.ncei.noaa.gov/thredds-ocean/catalog/ncei/wod/catalog.html

# cd to input directory
cd data

for year in {2000..2019}; do
    echo $year
    name="wod_ctd_"${year}".nc"
    filepath="https://www.ncei.noaa.gov/thredds-ocean/fileServer/ncei/wod/${year}/${name}"
    if [[ ! -f "$name" ]]; then
        wget --no-hsts $filepath
    fi
done

for year in {2000..2019}; do
    echo $year
    name="wod_osd_"${year}".nc"
    filepath="https://www.ncei.noaa.gov/thredds-ocean/fileServer/ncei/wod/${year}/${name}"
    if [[ ! -f "$name" ]]; then
        wget --no-hsts $filepath
    fi
done


for year in {2000..2019}; do
    echo $year
    name="wod_pfl_"${year}".nc"
    filepath="https://www.ncei.noaa.gov/thredds-ocean/fileServer/ncei/wod/${year}/${name}"
    if [[ ! -f "$name" ]]; then
        wget --no-hsts $filepath
    fi
done
